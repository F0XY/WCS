FROM python:3

WORKDIR /app

COPY . .
RUN pip install -r requirement.txt

ENTRYPOINT ["/bin/bash", "start-app.sh"]
