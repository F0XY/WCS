from loguru import logger
from logging import Handler

class LoguruHandlerForFlask(Handler):
    def emit(self, record):
        logger_opt = logger.opt(depth=6, exception=record.exc_info)
        logger_opt.log(record.levelno, record.getMessage())

