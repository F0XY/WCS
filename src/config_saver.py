config = {}

def config_from_module(cfg_module):
    global config

    for i in dir(cfg_module):
        if i.startswith("__"):
            continue

        config[i] = getattr(cfg_module, i)
