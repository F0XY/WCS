from sqlalchemy import LargeBinary, delete, event, insert
from sqlalchemy.engine import create_engine
from sqlalchemy.orm.session import sessionmaker, Session
from sqlalchemy.exc import SQLAlchemyError, NoResultFound
from typing import Callable, Any
from datetime import datetime
from loguru import logger
from ..config_saver import config

from .models.Base import Base
from .models.Role import Role
from .models.User import User
from .models.Product import Product
from .models.Order import Order
from .models.OrderProduct import OrderProduct
from .models.OrderCustom import OrderCustom
from .models.Bastket import Bastket

MAX_PRODUCT_ON_PAGE = 18
MAX_ORDER_ON_PAGE   = 5

class Database:
    def __init__(self, db_URI: str) -> None:
        global MAX_PRODUCT_ON_PAGE, MAX_ORDER_ON_PAGE
        MAX_PRODUCT_ON_PAGE = config.get("MAX_PRODUCT_ON_PAGE", 18)
        MAX_ORDER_ON_PAGE   = config.get("MAX_ORDER_ON_PAGE", 5)

        self.__engine = create_engine(db_URI)
        self.__sessionmaker = sessionmaker(self.__engine)

        self._run_in_session(lambda x: Base.metadata.create_all(self.__engine))

    def _run_in_session(self, func: Callable[[Any], Any], *args, **kwargs) -> Any:
        with self.__sessionmaker() as s:
            return func(s, *args, *kwargs)

    def db_query(func: Callable[[Any], Any]):
        def wrapper(self, *args, **kwargs) -> None:
            try:
                return self._run_in_session(func, *args, **kwargs)
            except NoResultFound:
                return None
            except SQLAlchemyError as err:
                logger.critical(f"SQLAlchemyError: {err.args}")
                return None
        return wrapper
    
    @event.listens_for(Role.__table__, 'after_create')
    def __init_default_role(table, connection, *args, **kwargs) -> None:
        connection.execute(insert(Role).values(name=config["CLIENT_ROLE_NAME"]))
        connection.execute(insert(Role).values(name=config["EMPLOYEE_ROLE_NAME"]))
    
    @db_query
    @staticmethod
    def get_product_image(s: Session, id: int) -> LargeBinary:
        return s.query(Product.logo).where(Product.id == id).one()

    @db_query
    @staticmethod
    def get_product_by_id(s: Session, id: int) -> Product:
        return s.query(Product).where(Product.id == id).one()

    @db_query
    @staticmethod
    def get_all_product(s: Session, page: int) -> list[Product]:
        return s.query(Product.id, Product.name).order_by(
            Product.name.asc()
        ).limit(MAX_PRODUCT_ON_PAGE).offset(MAX_PRODUCT_ON_PAGE*(page - 1)).all()
    
    @db_query
    @staticmethod
    def search_product(s: Session, query_str: str, page: int) -> list[Product]:
        return s.query(Product.id, Product.name).where(
            (Product.name + Product.description).like(f"%{query_str}%")
        ).limit(MAX_PRODUCT_ON_PAGE).offset(MAX_PRODUCT_ON_PAGE*(page - 1)).all()

    @db_query
    @staticmethod
    def search_product_count(s: Session, query_str: str) -> list[Product]:
        return s.query(Product.id).where((Product.name + Product.description).like(f"%{query_str}%")).count()
    
    @db_query
    @staticmethod
    def get_product_count(s: Session) -> int:
        return s.query(Product.id).count()

    @db_query
    @staticmethod
    def get_three_new_product(s: Session) -> list[Product]:
        return s.query(Product.id, Product.name).order_by(Product.date_of_add.desc()).limit(3).all()

    @db_query
    @staticmethod
    def get_user_by_login(s: Session, login: str) -> User:
        return s.query(User).where(User.login == login).one()
    
    @db_query
    @staticmethod
    def get_user_by_id(s: Session, id: int) -> User:
        return s.query(User).where(User.id == id).one()

    @db_query
    @staticmethod
    def add_user(s: Session, user: User) -> int:
        s.add(user)
        s.commit()

        return user.id

    @db_query
    @staticmethod
    def get_role_id_by_name(s: Session, role: str) -> int:
        return s.query(Role.id).where(Role.name == role).one()

    @db_query
    @staticmethod
    def get_orders_by_user_id(s: Session, user_id: int, page: int) -> list[Order]:
        return s.query(Order).where(Order.user_id == user_id).order_by(
            Order.date_begin.desc()
        ).limit(MAX_ORDER_ON_PAGE).offset(MAX_ORDER_ON_PAGE * (page - 1)).all()
    
    @db_query
    @staticmethod
    def get_orders_count_by_user_id(s: Session, user_id: int) -> int:
        return s.query(Order).where(Order.user_id == user_id).count()

    @db_query
    @staticmethod
    def get_product_from_basket(s: Session, user_id: int, product_id: int) -> Product:
        return s.query(Bastket).where((Bastket.user_id == user_id) & (Bastket.product_id == product_id)).one()

    @db_query
    @staticmethod
    def add_product_in_basket(s: Session, product_id: int, user_id: int) -> int:
        basket = Bastket()
        basket.product_id = product_id
        basket.user_id    = user_id
        basket.date_of_add = datetime.now()

        s.add(basket)
        s.commit()

        return basket.id

    @db_query
    @staticmethod
    def rm_product_from_basket(s: Session, product_id: int, user_id: int) -> None:
        bastket = s.query(Bastket).where((Bastket.user_id == user_id) & (Bastket.product_id == product_id)).one()
        s.delete(bastket)
        s.commit()

    @db_query
    @staticmethod
    def rm_product(s: Session, product_id: int) -> None:
        product = s.query(Product).where(Product.id == product_id).one()
        s.delete(product)
        s.commit()
    
    @db_query
    @staticmethod
    def get_all_baskert_by_user_id(s: Session, user_id: int, page: int) -> list[Bastket]:
        return s.query(Bastket).where(
            Bastket.user_id == user_id
        ).limit(MAX_PRODUCT_ON_PAGE).offset(MAX_PRODUCT_ON_PAGE * (page - 1)).all()
    
    @db_query
    @staticmethod
    def get_basket_count_by_user_id(s: Session, user_id: int) -> int:
        return s.query(Bastket).where(Bastket.user_id == user_id).count()

    @db_query
    @staticmethod
    def get_order_by_id(s: Session, order_id: int) -> Order:
        return s.query(Order).where(Order.id == order_id).one()

    @db_query
    @staticmethod
    def get_order_products(s: Session, order_id: int, page: int) -> list[Product]:
        return s.query(Product.id, Product.name).join(
                OrderProduct, OrderProduct.product_id == Product.id
        ).where(
            OrderProduct.order_id == order_id
        ).limit(MAX_PRODUCT_ON_PAGE).offset(MAX_PRODUCT_ON_PAGE * (page - 1)).all()

    @db_query
    @staticmethod
    def get_order_products_count(s: Session, order_id: int) -> int:
        return s.query(OrderProduct).where(order_id == OrderProduct.order_id).count()

    @db_query
    @staticmethod
    def get_requirements(s: Session, order_id: int) -> str:
        return s.query(OrderCustom.requirement).where(OrderCustom.order_id == order_id).one()

    @db_query
    @staticmethod
    def rm_all_products_from_basket(s: Session, user_id: int) -> None:
        q = delete(Bastket).where(user_id == Bastket.user_id)

        s.execute(q)
        s.commit()

    @db_query
    @staticmethod
    def add_order(s: Session, order: Order) -> int:
        s.add(order)
        s.commit()

        return order.id

    @db_query
    @staticmethod
    def add_order_product(s: Session, order_product: OrderProduct) -> int:
        s.add(order_product)
        s.commit()

        return order_product.id

    @db_query
    @staticmethod
    def add_order_custom(s: Session, order_custom: OrderCustom) -> int:
        s.add(order_custom)
        s.commit()

        return order_custom.id

    @db_query
    @staticmethod
    def get_all_products_id_and_price_in_bastket(s: Session, user_id: int) -> list[list[int, float]]:
        return s.query(Product.id, Product.price).join(Bastket, Product.id == Bastket.product_id).where(Bastket.user_id == user_id).all();

    @db_query
    @staticmethod
    def get_role_by_user_id(s: Session, user_id: int) -> Role:
        return s.query(Role).join(User, User.role == Role.id).where(User.id == user_id).one()

    @db_query
    @staticmethod
    def get_all_order(s: Session, page: int) -> list[Order]:
        return s.query(Order).order_by(
            Order.date_begin.desc()
        ).limit(MAX_PRODUCT_ON_PAGE).offset(MAX_PRODUCT_ON_PAGE * (page - 1)).all()

    @db_query
    @staticmethod
    def get_all_order_by_filter_by_status(s: Session, status: str, page: int) -> list[Order]:
        return s.query(Order).where(Order.status == status).order_by(
            Order.date_begin.desc()
        ).limit(MAX_PRODUCT_ON_PAGE).offset(MAX_PRODUCT_ON_PAGE * (page - 1)).all()
    
    @db_query
    @staticmethod
    def get_all_order_by_filter_by_status_count(s: Session, status: str) -> list[Order]:
        return s.query(Order).where(Order.status == status).count()
    
    @db_query
    @staticmethod
    def get_all_order_count(s: Session) -> int:
        return s.query(Order).count()

    @db_query
    @staticmethod
    def update_order(s: Session, order: Order) -> None:
        s.merge(order)
        s.commit()
    
    @db_query
    @staticmethod
    def add_product(s: Session, product: Product) -> int:
        s.add(product)
        s.commit()

        return product.id

    @db_query
    @staticmethod
    def update_product(s: Session, product: Product) -> int:
        s.merge(product)
        s.commit()

        return product.id
