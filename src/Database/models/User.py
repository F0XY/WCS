from .Base import Base
from .Role import Role
from sqlalchemy import Column, Integer, String, ForeignKey
from ...utils import generateSalt, string_hashing

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, nullable=False)
    role = Column(Integer, ForeignKey(Role.id, ondelete="CASCADE", onupdate="CASCADE"), nullable=False)

    firstname = Column(String(45), nullable=False)
    lastname  = Column(String(45), nullable=False)
    surname   = Column(String(45))

    login    = Column(String(45), nullable=False)
    password = Column(String(45), nullable=False)
    salt     = Column(String(20), nullable=False)

    phone = Column(String(20))
    email = Column(String(100)) 
    
    def set_password(self, password: str) -> None:
        self.salt     = generateSalt()
        self.password = string_hashing(password + self.salt)

    def check_password(self, password: str) -> bool:
        return self.password == string_hashing(password + self.salt)

