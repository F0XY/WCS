from .Base import Base
from sqlalchemy import Column, Integer, String, Float, LargeBinary, DateTime

class Product(Base):
    __tablename__ = "product"

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(45), nullable=False)
    description = Column(String(255), nullable=False)
    logo = Column(LargeBinary(length=2 ** 32 - 1), nullable=False)
    price = Column(Float, nullable=False)
    date_of_add = Column(DateTime, nullable=False)
