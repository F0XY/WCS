from .Base import Base
from .User import User
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey

class Order(Base):
    __tablename__ = "order"

    id = Column(Integer, primary_key=True, nullable=False)
    user_id = Column(Integer, ForeignKey(User.id, ondelete="CASCADE", onupdate="CASCADE"), nullable=False)

    date_begin = Column(DateTime, nullable=False)
    date_end   = Column(DateTime)

    status = Column(String(45), nullable=False)
    price  = Column(Integer)
