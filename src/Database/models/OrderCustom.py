from .Base import Base
from .Order import Order
from sqlalchemy import Column, Integer, ForeignKey, String

class OrderCustom(Base):
    __tablename__ = "order_custom"

    id = Column(Integer, primary_key=True, nullable=False)
    order_id    = Column(Integer, ForeignKey(Order.id, ondelete="CASCADE", onupdate="CASCADE"), nullable=False)
    requirement = Column(String(500), nullable=False)
