from .Base import Base
from .User import User
from .Product import Product
from sqlalchemy import Column, Integer, DateTime, ForeignKey

class Bastket(Base):
    __tablename__ = "basket"

    id = Column(Integer, primary_key=True, nullable=False)
    user_id    = Column(Integer, ForeignKey(User.id, ondelete="CASCADE", onupdate="CASCADE"), nullable=False)
    product_id = Column(Integer, ForeignKey(Product.id, ondelete="CASCADE", onupdate="CASCADE"), nullable=False)

    date_of_add = Column(DateTime, nullable=False)
