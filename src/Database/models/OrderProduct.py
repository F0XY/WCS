from .Base import Base
from .Order import Order
from .Product import Product
from sqlalchemy import Column, Integer, ForeignKey

class OrderProduct(Base):
    __tablename__ = "order_product"

    id = Column(Integer, primary_key=True, nullable=False)
    order_id = Column(Integer, ForeignKey(Order.id, onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    product_id = Column(Integer, ForeignKey(Product.id, ondelete="CASCADE", onupdate="CASCADE"),  nullable=False)
