from flask import Flask, Blueprint, session
from loguru import logger
from datetime import timedelta
from .Loggins import LoguruHandlerForFlask
from .Database.Database import Database
from .blueprints.client.client import bp_client
from .blueprints.employee.employee import bp_employee
from .config_saver import config

class Application(Flask):
    def __init__(self, db_uri: str, secret_key: str, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        
        logger.start("test.log", format="[{time}]\t[{level}]\t- {message}")
        self.logger.addHandler(LoguruHandlerForFlask())

        self.__db = Database(config["DATABASE_URI"])
        self.config["SECRET_KEY"] = config["SECRET_KEY"]
        
        self.before_request(self.__before_request_func)

        self.addBlueprint(bp_client)
        self.addBlueprint(bp_employee, url_prefix="/employee")

    def addBlueprint(self, blueprint: Blueprint, **option) -> None:
        self.register_blueprint(blueprint, **option)

    def getDatabase(self) -> Database:
        return self.__db

    def __before_request_func(self) -> None:
        session.permanent = True
        self.permanent_session_lifetime = timedelta(days=1)
