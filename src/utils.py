from flask import url_for, session, current_app
from sys import argv
from os import path
from hashlib import md5, sha256
from string import ascii_letters, digits, punctuation
from random import choice
from flask import request

ABS_PATH = path.split(path.abspath(argv[0]))[0]

def generate_page(endpoint: str, min_page: int, max_page: int, *url_for_args, **url_for_kwargs) -> dict[str, int] | None:
    pages = {"current_page": 1}
    args  = dict(request.args)

    if "page" in args:
        try:
            pages["current_page"] = int(request.args["page"])
        except ValueError:
            return None

        del args["page"]

    if pages["current_page"] > min_page:
        pages["prev_page"] = url_for(endpoint, *url_for_args, **args, **url_for_kwargs, page=pages["current_page"] - 1)

    if pages["current_page"] < max_page:
        pages["next_page"] = url_for(endpoint, *url_for_args, **args, **url_for_kwargs, page=pages["current_page"] + 1)

    return pages

def main_account() -> str | None:
    user_id = session.get("user_id", None)
    if not user_id:
        return None

    db: Database = current_app.getDatabase()
    user = db.get_user_by_id(user_id)

    return f"{user.lastname} {user.firstname[0]}.{user.surname[0]}."


def string_hashing(data: str) -> str:
    hash_data = sha256(data.encode()).hexdigest()
    hash_data = md5(hash_data.encode()).hexdigest()
    return hash_data

def generateSalt(length: int = 16):
    chars = ascii_letters + digits + punctuation
    salt = ''.join(choice(chars) for _ in range(length))
    return salt

