from flask import Blueprint, current_app, abort, session, render_template, request, redirect, url_for
from ...Database.Database import Database, MAX_ORDER_ON_PAGE, MAX_PRODUCT_ON_PAGE, Product
from ...config_saver import config
from ...utils import generate_page, main_account
from base64 import b64encode
from math import ceil
from datetime import datetime

bp_employee = Blueprint("BP_EMPLOYEE", __name__)

@bp_employee.route("/")
def list_order():
    if not check_is_employee():
        return abort(403)
    
    db: Database = current_app.getDatabase()
    
    status = request.args.get("status", None)
    if status:
        count = db.get_all_order_by_filter_by_status_count(status)
    else:
        count = db.get_all_order_count()

    max_page = max(ceil(count / MAX_ORDER_ON_PAGE), 1)

    pages = generate_page("BP_EMPLOYEE.list_order", 1, max_page)
    if not pages:
        return abort(404)
    
    orders = []
    if status:
        orders = db.get_all_order_by_filter_by_status(status, pages["current_page"])
    else:
        orders = db.get_all_order(pages["current_page"])
    
    return render_template("order-list.html",
        title="Список заявок",
        account_fullname=main_account(),
        css_files=(
            "search-box.css",
            "order.css",
            "list.css"
        ),
        is_employee=True,
        orders=orders,
        **pages
    )

@bp_employee.route("/order/<int:order_id>", methods=["GET", "POST"])
def order(order_id):
    if not check_is_employee():
        return abort(403)

    db: Database = current_app.getDatabase()
    
    order = db.get_order_by_id(order_id)
    if not order:
        return abort(404)

    client = db.get_user_by_id(order.user_id)

    if request.method == "POST":
        price = float(request.form["price"])
        status = request.form["status"]

        order.price = price
        order.status = status

        if status == "Завершено":
            order.date_end = datetime.now()
        else:
            order.date_end = None

        db.update_order(order)
        
    count = db.get_order_products_count(order_id)
    max_page = max(ceil(count / MAX_PRODUCT_ON_PAGE), 1)

    pages = generate_page("BP_EMPLOYEE.order", 1, max_page, order_id=order_id)
    if not pages:
        abort(404)
    
    products = db.get_order_products(order_id, pages["current_page"])
    requirements = ""
    if not products:
        requirements = db.get_requirements(order_id)[0]
    
    return render_template("order.html",
        title=f"Заказ №{order.id}",
        account_fullname=main_account(),
        css_files=(
            "order.css",
            "list.css",
            "form.css",
            "products.css"
        ),
        edit=True,
        order=order,
        user_fullname=f"{client.lastname} {client.firstname[0]}.{client.surname[0]}.",
        user_id=client.id,
        products=products,
        requirements=requirements,
        **pages
    )

@bp_employee.route("/products", methods=["GET", "POST"])
def products():
    if not check_is_employee():
        return abort(403)

    db: Database = current_app.getDatabase()

    if request.method == "POST":
        product_id = request.form["product_id"]
        db.rm_product(product_id)
    
    search_query: str = request.args.get("search-query", None)
    if search_query:
        search_query = search_query.strip()
        count = db.search_product_count(search_query)
    else:
        count = db.get_product_count()

    max_page = max(ceil(count / MAX_PRODUCT_ON_PAGE), 1)

    pages = generate_page("BP_CLIENT.gallery", 1, max_page)
    if not pages:
        return abort(404)
    
    products = []
    if search_query:
        products = db.search_product(search_query, pages["current_page"])
    else:
        products = db.get_all_product(pages["current_page"])

    return render_template("gallery.html",
        title="Галерея",
        account_fullname=main_account(),
        css_files=(
            "products.css",
            "search-box.css",
            "list.css",
            "form.css"
        ),
        search_query=search_query,
        products=products,
        edit=True,
        **pages
    )


@bp_employee.route("/product/add", methods=["GET", "POST"])
def product_add():
    if not check_is_employee():
        return abort(403)

    error_message = []

    if request.method == "POST":
        file = request.files["image"]
        data = file.read()
        if len(data) > 2**32 - 1:
            error_message.append("изображение слишком бальшое!")
        else:
            product = Product()
            product.name = request.form["name"]
            product.price = float(request.form["price"])
            product.description = request.form["descriptions"]
            product.date_of_add = datetime.now()
            
            product.logo = data

            db: database = current_app.getDatabase()
            product_id = db.add_product(product)

            if product:
                return redirect(url_for("BP_EMPLOYEE.products"))
            else:
                error_message.append("не известная ошибка! попробуйте позже...")
        

    return render_template("product-form.html",
        title="Добавление продукции",
        css_files=(
            "form.css",
        ),
        error_messages=error_message
    )

@bp_employee.route("/product/<int:id>/edit", methods=["GET", "POST"])
def product_edit(id):
    if not check_is_employee():
        return abort(403)
    
    db: Database = current_app.getDatabase()
    product = db.get_product_by_id(id)

    error_message = []

    if request.method == "POST":
        file = request.files["image"]
        data = file.read()
        if len(data) > 2**32 - 1:
            error_message.append("изображение слишком бальшое!")
        else:
            product.name = request.form["name"]
            product.price = float(request.form["price"])
            product.description = request.form["descriptions"]
            product.date_of_add = datetime.now()
            
            product.logo = data

            db: database = current_app.getDatabase()
            product_id = db.update_product(product)

            if product:
                return redirect(url_for("BP_EMPLOYEE.products"))
            else:
                error_message.append("не известная ошибка! попробуйте позже...")


    return render_template("product-form.html",
        title="Редактирование продуцкии",
        css_files=(
            "form.css",
        ),
        product=product
    )

@bp_employee.route("/account/<int:id>")
def client(id):
    if not check_is_employee():
        return abort(403)

    db: Database = current_app.getDatabase()
    user = db.get_user_by_id(id)

    count = db.get_orders_count_by_user_id(id)
    max_page = max(ceil(count / MAX_ORDER_ON_PAGE), 1)

    pages = generate_page("BP_CLIENT.my_account", 1, max_page)
    if not pages:
        return abort(404)
    
    orders = db.get_orders_by_user_id(id, pages["current_page"])

    return render_template("account.html",
        title=f"Аккуант пользователя №{id}",
        account_fullname=main_account(),
        css_files=(
            "account.css",
            "order.css",
            "list.css"
        ),
        fullname=f"{user.lastname} {user.firstname} {user.surname}",
        email=user.email,
        phone=user.phone,
        orders=orders,
        **pages
    )

def check_is_employee() -> bool:
    user_id = session.get("user_id", None)
    if not user_id:
        return False

    db: Database = current_app.getDatabase()
    role = db.get_role_by_user_id(user_id)

    return role.name == config["EMPLOYEE_ROLE_NAME"]
