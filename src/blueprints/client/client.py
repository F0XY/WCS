from flask import Blueprint, render_template, request, url_for, abort, current_app, send_file, session, redirect
from io import BytesIO
from ...utils import ABS_PATH, generate_page, main_account
from ...Database.Database import Database, MAX_PRODUCT_ON_PAGE, MAX_ORDER_ON_PAGE, User, OrderProduct, OrderCustom, Order, Bastket
from ...config_saver import config
from math import ceil
from datetime import datetime

bp_client = Blueprint('BP_CLIENT', __name__, template_folder=ABS_PATH+"/templates")

@bp_client.route("/")
def main():
    db: Database = current_app.getDatabase()
    
    products = db.get_three_new_product()
    if not products:
        products = []

    return render_template("index.html",
        title="Главная страница",
        account_fullname=main_account(),
        css_files=(
            "main-content.css",
            "products.css"
        ),
        products=products
    )

@bp_client.route("/gallery", methods=["POST", "GET"])
def gallery():
    db: Database = current_app.getDatabase()
    
    search_query: str = request.args.get("search-query", None)
    if search_query:
        search_query = search_query.strip()
        count = db.search_product_count(search_query)
    else:
        count = db.get_product_count()

    max_page = max(ceil(count / MAX_PRODUCT_ON_PAGE), 1)

    pages = generate_page("BP_CLIENT.gallery", 1, max_page)
    if not pages:
        return abort(404)
    
    products = []
    if search_query:
        products = db.search_product(search_query, pages["current_page"])
    else:
        products = db.get_all_product(pages["current_page"])

    return render_template("gallery.html",
        title="Галерея",
        account_fullname=main_account(),
        css_files=(
            "products.css",
            "search-box.css",
            "list.css"
        ),
        search_query=search_query,
        products=products,
        **pages
    )

@bp_client.route("/registration", methods=["GET", "POST"])
def registration():
    if "user_id" in session:
        return redirect(url_for('BP_CLIENT.my_account'))

    error_messages = []
    if request.method == "POST":
        firstname =  request.form["firstname"].strip()
        lastname  = request.form["lastname"].strip()
        surname   = request.form["surname"].strip()

        email = request.form.get("email", None)
        phone = request.form.get("phone", None)

        login           = request.form["login"].strip()
        password        = request.form["password"].strip()
        password_repeat = request.form["password_repeat"].strip()
        
        db: Database = current_app.getDatabase()
        if db.get_user_by_login(login):
            error_messages.append("Данный логин уже занят!")

        if not email and not phone:
            error_messages.append("Должен быть заполнено хоть одно контактное поле!")

        if password_repeat != password:
            error_messages.append("Пароли не совподают!")

        if not error_messages:
            role_id = db.get_role_id_by_name(config["CLIENT_ROLE_NAME"])[0]

            user = User()
            user.firstname = firstname
            user.lastname = lastname
            user.surname = surname
            user.role  = role_id
            user.email = email
            user.phone = phone
            user.login = login
            user.set_password(password)

            user_id = db.add_user(user)
            if user_id:
                session["user_id"] = user_id

                prev_url = session.pop("prev_url", None)
                if prev_url:
                    return redirect(prev_url)

                return redirect(url_for("BP_CLIENT.my_account"))
            error_messages.append("Произошла внутрняя ошибка! Попробуйте позже...")

    return render_template("registration.html", 
        title="Регистрация",
        account_fullname=main_account(),
        css_files=(
            "form.css",
        ),
        error_messages=error_messages
    )

@bp_client.route("/login", methods=["GET", "POST"])
def login():
    if "user_id" in session:
        return redirect(url_for('BP_CLIENT.my_account'))
    
    error_message=None
    if request.method == "POST":
        login = request.form["login"].strip()
        password = request.form["password"].strip()

        db: Database = current_app.getDatabase()
        user = db.get_user_by_login(login)
        
        if not user or not user.check_password(password):
            error_message = "Неверный логин или пароль"
        
        if not error_message:
            session["user_id"] = user.id

            prev_url = session.pop("prev_url", None)
            if prev_url:
                return redirect(prev_url)

            return redirect(url_for("BP_CLIENT.my_account"))

    return render_template("login.html",
        title="Авторизация",
        account_fullname=main_account(),
        css_files = (
            "form.css",
        ),
        error_message=error_message
    )

@bp_client.route("/my_account")
def my_account():
    user_id = session.get("user_id", None)
    if not user_id:
        return redirect(url_for("BP_CLIENT.login"))

    db: Database = current_app.getDatabase()
    user = db.get_user_by_id(user_id)
    role = db.get_role_by_user_id(user_id)
    
    is_employee = role.name == config["EMPLOYEE_ROLE_NAME"]

    count = db.get_orders_count_by_user_id(user_id)
    max_page = max(ceil(count / MAX_ORDER_ON_PAGE), 1)

    pages = generate_page("BP_CLIENT.my_account", 1, max_page)
    if not pages:
        return abort(404)
    
    orders = db.get_orders_by_user_id(user_id, pages["current_page"])

    return render_template("my_account.html",
        title="Мой аккаунт",
        account_fullname=main_account(),
        css_files=(
            "account.css",
            "order.css",
            "list.css"
        ),
        fullname=f"{user.lastname} {user.firstname} {user.surname}",
        email=user.email,
        phone=user.phone,
        orders=orders,
        is_employee=is_employee,
        **pages
    )

@bp_client.route("/my_account/exit")
def account_exit():
    session.pop("user_id", None)
    return redirect(url_for("BP_CLIENT.main"))

@bp_client.route("/product/<int:id>", methods=["GET", "POST"])
def product(id: int):
    db: Database = current_app.getDatabase()

    product = db.get_product_by_id(id)
    user_id = session.get("user_id", None)

    if not product:
        return abort(404)

    if request.method == "POST":
        if not user_id:
            session["prev_url"] = request.path
            return redirect(url_for("BP_CLIENT.login"))

        if "add_product" in request.form:
            db.add_product_in_basket(product.id, user_id)
        elif "rm_product" in request.form:
            db.rm_product_from_basket(product.id, user_id)
        elif "buy" in request.form:
            order_id = add_product_order(user_id, [[product.id, product.price]])
            return redirect(url_for("BP_CLIENT.order", order_id=order_id))
    
    product_in_basket = False
    if user_id and db.get_product_from_basket(user_id, product.id):
        product_in_basket = True

    return render_template("product.html",
        title=product.name,
        account_fullname=main_account(),
        css_files=(
            "account.css",
            "form.css",
        ),
        product=product,
        product_in_basket=product_in_basket
    )

@bp_client.route("/product/<int:id>/img")
def product_img(id: int):
    db: Database = current_app.getDatabase()
    
    img = db.get_product_image(id)
    if not img:
        return abort(404)

    img = BytesIO(img[0])
    return send_file(img, mimetype="image/png")

@bp_client.route("/bastket", methods=["GET", "POST"])
def bastket():
    user_id = session.get("user_id", None)
    if not user_id:
        session["prev_url"] = request.path
        return redirect(url_for("BP_CLIENT.login"))
    
    db: Database = current_app.getDatabase()
    if request.method == "POST":
        if "rm-product" in request.form:
            product_id = int(request.form["product_id"])
            db.rm_product_from_basket(product_id, user_id)
        elif "rm-all" in request.form:
            db.rm_all_products_from_basket(user_id) 
        elif "buy-all" in request.form:
            products = db.get_all_products_id_and_price_in_bastket(user_id)
            if products:
                order_id = add_product_order(user_id, products)
                db.rm_all_products_from_basket(user_id)

                return redirect(url_for("BP_CLIENT.order", order_id=order_id))

    count = db.get_basket_count_by_user_id(user_id)
    max_page = max(ceil(count / MAX_PRODUCT_ON_PAGE), 1)

    pages = generate_page("BP_CLIENT.bastket", 1, max_page)
    if not pages:
        return abort(404)
    
    bastket = db.get_all_baskert_by_user_id(user_id, pages["current_page"])

    products = []
    if bastket:
        for i in bastket:
            product = db.get_product_by_id(i.product_id)
            products.append((product.id, product.name))

    return render_template("bastket.html",
        title="Корзина",
        account_fullname=main_account(),
        css_files=(
            "products.css",
            "search-box.css",
            "list.css",
            "form.css"
        ),
        products=products,
        is_bastket=True,
        **pages
    )

@bp_client.route("/custom_order", methods=["GET", "POST"])
def custom_order():
    user_id = session.get("user_id", None)
    
    if not user_id:
        session["prev_url"] = request.path
        return redirect(url_for("BP_CLIENT.login"))
    
    if request.method == "POST":
        db: Database = current_app.getDatabase()

        requirements = request.form["requirements"]

        order = Order()
        order.user_id = user_id
        order.status = "В ожидании"
        order.date_begin = datetime.now()

        order_id = db.add_order(order)

        order_custom = OrderCustom()
        order_custom.order_id = order_id
        order_custom.requirement = requirements

        db.add_order_custom(order_custom)

        return redirect(url_for("BP_CLIENT.order", order_id=order_id))

    return render_template("custom-order.html",
        title="Персональный заказ",
        account_fullname=main_account(),
        css_files=(
            "form.css",
        )
    )

@bp_client.route("/order/<int:order_id>")
def order(order_id: int):
    user_id = session.get("user_id", None)
    if not user_id:
        session["prev_url"] = request.path
        return redirect(url_for("BP_CLIENT.login"))

    db: Database = current_app.getDatabase()
    
    order = db.get_order_by_id(order_id)
    if not order:
        return abort(404)

    if order.user_id != user_id:
        return abort(403)

    count = db.get_order_products_count(order_id)
    max_page = max(ceil(count / MAX_PRODUCT_ON_PAGE), 1)

    pages = generate_page("BP_CLIENT.order", 1, max_page, order_id=order_id)
    if not pages:
        abort(404)
    
    products = db.get_order_products(order_id, pages["current_page"])
    requirements = ""
    if not products:
        requirements = db.get_requirements(order_id)[0]

    return render_template("order.html",
        title=f"Заказ №{order.id}",
        account_fullname=main_account(),
        css_files=(
            "order.css",
            "list.css",
            "products.css"
        ),
        edit=False,
        order=order,
        products=products,
        requirements=requirements,
        **pages
    )

@bp_client.app_errorhandler(404)
def not_found_error(e):
    return render_template("error.html",
        title="Ничего не найдено!",
        css_files=(
            "404.css",
        ),
        error_code=404,
        error_message="По вашему запросу ничего не найдено!"
    ), 404

@bp_client.app_errorhandler(403)
def forbidden_error(e):
    return render_template("error.html",
        title="Ошибка доступа",
        css_files=("404.css",),
        error_code=403,
        error_message="У вас нет досупа к данной странице!"
    ), 403

def add_product_order(user_id: int, products: list[list[int, float]]) -> int:
    db: Database = current_app.getDatabase()
    
    order = Order()
    order.user_id = user_id
    order.status = "В ожидании"
    order.date_begin = datetime.now()
    order.price = sum([ i[1] for i in products ])

    order_id = db.add_order(order)

    for i in products:
        order_product = OrderProduct()
        order_product.order_id = order_id
        order_product.product_id = i[0]

        db.add_order_product(order_product)

    return order_id
