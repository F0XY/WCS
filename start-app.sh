
uwsgi_bin=( $(find /usr -type f -name "uwsgi" 2> /dev/null) )

if [[ "$USE_HTTP_PROTOCOL" == "ON" ]]; then
    ${uwsgi_bin[0]} --socket 0.0.0.0:8080 -p 5 --protocol=http -w main:app
else
    ${uwsgi_bin[0]} --socket 0.0.0.0:8080 -p 5 -w main:app
fi
