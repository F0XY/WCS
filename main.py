from src import *
import config as cfg

config_from_module(cfg)
app = Application(cfg.DATABASE_URI, cfg.SECRET_KEY, __name__)

if __name__ == "__main__":
    app.run(debug=True)
