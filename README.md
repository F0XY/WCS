# WCS

Курсовая деятельность по предметной области "Магизин кондитерских изделий"

Данный Web-сайт разработан для виртуальной организации "Сладкий мир", у которой появилась неопходимость в разработке Web-сайта для организаци заказа продукции через сети интернет для клиентов, а акткжу упращения раюоты с документами заказов.

## Установка

### Предусловие

На итоговом устройстве должен быть установлен Python 3.11 или выше, система контроля версий Git, система контейнерации Docker (Не обязательно) и любой HTTP сервер с поддержкой протокола WSGI.

### Установка из репозитория

Установка Web-сайта на ваш компьютер с удаленного репозитория.

```bash
git clone https://codeberg.org/F0XY/WCS
cd WCS
```
### Установка без системы контейнеризации

#### Создание и запуск виртуальной среды

##### Создание виртупльной среды
Данный шаг является не обязательным, но рекумендуемым для изоляции программного обеспечения от внешней операционной системы, для того что бы не вызывать конфликты версий модулей, выступующие зависимостями.
```bash
python -m venv venv
```

##### Запуск виртуальной среды
Запуск виртуальной среды отличается в зависимости от операционной системы

Для ОС семейства `Unix`:
```bash
source ./venv/bin/activate
```

Для ОС семейста `Windows`:
```bash
.\\venv\\Scripts\\activate.bat
```

#### Установка зависимостей

Осоновными зависимостями являются:
 * Flask
 * SQLalchemy
 * Loguru
 * Jinja2
 * uWSGI

##### Ручная установка зависимостей

```bash
pip install -r requirement.txt
```

### Установка через контенер

Для этого варианта установки у вас должен быть предустановлен `Docker` в вашу операционную систему.
Установка через контейнерезацию представляет из себя автоматическую сборку контейнера для программного продукта.

Имя контейнера пишется в формате `snake_case`.

```bash
docker build -t <имя итогового контейнера> .
```

## Конфигурация

Конфигурация программного продукта представляется в виде настройки переменных в файле `config.py`

```python
DATABASE_URI = "<Драйвер базы данных>://<Имя пользователя>:<Пароль>@<IP или имя контейера>[:<порт>]/<Имя базы данных>"
SECRET_KEY   = "<Секретный код для FLASK>"

MAX_PRODUCT_ON_PAGE = 18 # Максимальное количество отображаемых на странице карточек продуктов
MAX_ORDER_ON_PAGE   = 5  # Максимальное количество отображаемых на странице карточек заказов

CLIENT_ROLE_NAME = "клиент" # Стандартное наиманование роли клиентов организации (Неопходимо только при развертывании базы данных)
EMPLOYEE_ROLE_NAME = "менеджер" # Стандартное наиманование роли сотрудников организации (Неопходимо только при развертывании базы данных)
```

## Запуск

### Запуск из виртуальной среды

#### В режиме WSGI сервера
Данный вид запуска требует предустновленный HTTP сервер с поддрежкой WSGI, к примеру `Apache` или `Nginx`
```bash
uwsgi --socket <IP>[:Порт] -w main:app
```

#### В режиме HTTP сервера
Данный способ рекомендум только для тестовых запуском системы
```bash
uwsgi --socket <IP>[:Порт] --protocol=http -w main:app
```

## Авторы

Рябинин Д. В. - Псевдонимы: F0XY, CubeCompanion - Инициатор работы - [https://vk.com/cxxcoder](ВК)

